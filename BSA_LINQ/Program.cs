﻿using System;
using System.Threading.Tasks;

namespace BSA.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await new Menu().GetMainMenu();
        }
    }
}
