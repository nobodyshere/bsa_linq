﻿using System;
using System.Threading.Tasks;

namespace BSA.Client
{
    class Menu
    {
        private bool run = true;
        DataProvider data = DataProvider.GetInstance();
        CrudMenu crudMenu = CrudMenu.GetInstance();

        public async Task GetMainMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Please, press any key to continue.");
            Console.ResetColor();

            while (run)
            {
                ShowMenu();
                await GetInput();
            }
        }

        private void ShowMenu()
        {
            Console.ReadKey();
            Console.Clear();
            var currentColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Please choose an option below");
            Console.ForegroundColor = currentColor;

            Console.WriteLine("1. Creadte/Read/Update/Delete objects \n\n" +
                "2. Get the number of tasks for a specific user project (by id) \n\n" +
                "3. Get a list of tasks assigned to a specific user (by id), where name less than 45 characters \n\n" +
                "4. Get a list (id, name) from the collection of tasks that are completed (finished) in the current (2019) year for a specific user (by id) \n\n" +
                "5. Get a list (id, team name and list of users) from a collection of teams that are over 12 years old, sorted by user registration date in descending order, and also grouped by teams. \n\n" +
                "6. Get a list of users alphabetically first_name (ascending) with sorted tasks by name (descending) \n\n" +
                "7. Get the following structure: User, User’s last project (by creation date), Total number of tasks under the last project, Total number of incomplete or canceled tasks for a user, User’s longest task by date (first created, later completed)\n" +
                "8. Get the following structure: Project, The longest task of the project(by description), Total number of users in the project team, where either the project description is less than 25 characters or the number of tasks is less than 3\n\n" +
                "9. Add User \n" +
                "0 (Enter). Exit \n");
        }

        private async Task GetInput()
        {
            int.TryParse(Console.ReadLine(), out int responce);

            Console.Clear();
            switch (responce)
            {
                case 0:
                    run = false;
                    break;
                case 1:
                    crudMenu.MenuStart();
                    break;
                case 2:
                    await GetNumberOfTasks();
                    break;
                case 3:
                    await GetListOfTasks();
                    break;
                case 4:
                    await GetListOfFinishedTasks();
                    break;
                case 5:
                    await GetListOfOlderTeams();
                    break;
                case 6:
                    await GetListOfSortedUsers();
                    break;
                case 7:
                    await GetUserTasksInfo();
                    break;
                case 8:
                    await GetTasksInfoForProject();
                    break;
                default:
                    DisplayError($"There are no option like {responce}.");
                    ShowMenu();
                    break;
            }
        }

        private async Task GetUserTasksInfo()
        {
            Console.WriteLine("Please, enter user id");
            string respoce = Console.ReadLine();

            int userID = Convert.ToInt32(respoce);
            var tasks = await data.GetUserTasksData(userID);

            Console.WriteLine($"User ID: {tasks.user.Id}, Full Name: {tasks.user.FirstName} {tasks.user.LastName}\n" +
                $"Last Project ID: {tasks.lastProject.Id}, Name: {tasks.lastProject.Name}\n" +
                $"Tasks in last project: {tasks.lastProjectTasksCount}\n" +
                $"Unfinished tasks in last project: {tasks.unfinishedTasksCount}\n" +
                $"Longest task by date id: {tasks.longestTaskByDate.Id}, Created at: {tasks.longestTaskByDate.CreatedAt.ToShortDateString()}, Finished at: {tasks.longestTaskByDate.FinishedAt.ToShortDateString()}");

        }

        private async Task GetTasksInfoForProject()
        {
            Console.WriteLine("Please, enter project id");
            string responce = Console.ReadLine();

            int projectID = Convert.ToInt32(responce);

            var tasks = await data.GetTasksInfoForProject(projectID);

            if (tasks != null)
            {
                Console.WriteLine($"Project ID: {tasks.project.Id}, Project Name: {tasks.project.Name}");
                if (tasks.longestTask != null)
                {
                    Console.WriteLine($"Longest task of project (by description): {tasks.longestTask.Id} \n" +
                        $"With description: {tasks.longestTask.Description}");

                    Console.WriteLine($"The shortest task of project (by name): {tasks.shortestTask.Id} \n" +
                        $"With name: {tasks.shortestTask.Name}");
                }
                else
                {
                    Console.WriteLine("There are no tasks in this project");
                }

                Console.WriteLine("Total number of users in the project team, where either the project description is less than 25 characters or the number of tasks is less than 3:");
                Console.WriteLine(tasks.usersCount);
            }
            else
            {
                DisplayError("Project not found");
            }
        }

        private async Task GetListOfSortedUsers()
        {
            var list = await data.GetSortedUsers();

            foreach (var item in list)
            {
                Console.WriteLine($"User ID: {item.user.Id}, Full Name: {item.user.FirstName} {item.user.LastName}, Tasks list:");
                foreach (var x in item.tasks)
                {
                    Console.WriteLine($"{x.Id}, {x.Name}");
                }

                Console.WriteLine();
            }
        }

        private async Task GetListOfOlderTeams()
        {
            var teams = await data.GetListOfOlderUsers();

            foreach (var item in teams)
            {
                Console.WriteLine($"Team ID: {item.teamId}, Team Name: {item.teamName}, List of users:");
                foreach (var x in item.users)
                {
                    Console.WriteLine($"ID: {x.Id}, Full name: {x.FirstName} {x.LastName}, Birth: {x.Birthday}");
                }

                Console.WriteLine();
            }
        }

        private async Task GetListOfFinishedTasks()
        {
            Console.WriteLine("Please, enter user id");
            string respoce = Console.ReadLine();

            int userID = Convert.ToInt32(respoce);
            var tasks = await data.GetFinishedTasks(userID);

            if (tasks.Count != 0)
            {
                Console.WriteLine($"In this year user with id {userID} has finish next tasks:");
                foreach (var item in tasks)
                {
                    Console.WriteLine($"Task ID: {item.Key}, Task Name: {item.Value}");
                }
            }
            else
            {
                Console.WriteLine("This user dont have finished tasks in 2019");
            }

        }

        private async Task GetListOfTasks()
        {
            Console.WriteLine("Please, enter user id");
            string respoce = Console.ReadLine();

            int userID = Convert.ToInt32(respoce);
            var tasks = await data.GetListOfUserTasks(userID);

            Console.WriteLine($"User with id {userID} have next assigned tasks:"); ;
            foreach (var item in tasks)
            {
                Console.WriteLine($"Task ID: {item.Id}, Name: {item.Name}");
            }
        }

        private async Task GetNumberOfTasks()
        {
            Console.WriteLine("Please, enter user id");
            string respoce = Console.ReadLine();

            int userID = Convert.ToInt32(respoce);
            var dictionary = await data.GetNumberOfUserTasks(userID);

            if (dictionary.Count > 0)
            {
                Console.WriteLine($"User with id {userID} have next projects with tasks:");
                foreach (var item in dictionary)
                {
                    Console.WriteLine($"Project ID: {item.Key.Id} with {item.Value} tasks");
                }
            }
            else
            {
                Console.WriteLine($"User with id {userID} dont have any projects with tasks");
            }
        }

        private void DisplayError(string message)
        {
            var current = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write($"ERROR: ");
            Console.ForegroundColor = current;
            Console.WriteLine(message);
        }
    }
}
