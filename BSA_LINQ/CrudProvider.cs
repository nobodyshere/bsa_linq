﻿using BSA.Client.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BSA.Client
{
    public class CrudProvider
    {
        private static readonly Lazy<CrudProvider> Instance = new Lazy<CrudProvider>(() => new CrudProvider());

        public static CrudProvider GetInstance() => Instance.Value;

        const string route = "https://localhost:44344/api/";
        HttpClient http;

        private CrudProvider()
        {
            http = new HttpClient();
            http.BaseAddress = new Uri(route);
            http.DefaultRequestHeaders.Accept.Clear();
            http.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<HttpResponseMessage> CreateProject(ProjectDTO project)
        {
            var jsoncontent = JsonConvert.SerializeObject(project);
            var content = new StringContent(jsoncontent, Encoding.UTF8, "application/json");
            return await http.PostAsync($"{route}projects/", content);
        }

        public async Task<ProjectDTO> GetProject(int id)
        {
            ProjectDTO project;
            var response = await http.GetAsync($"{route}projects/{id}");
            project = JsonConvert.DeserializeObject<ProjectDTO>(await response.Content.ReadAsStringAsync());
            return project;
        }

        public async Task<HttpResponseMessage> UpdateProject(ProjectDTO project, int id)
        {
            var content = JsonConvert.SerializeObject(project);
            return await http.PutAsJsonAsync($"{route}projects/{id}", project);
        }

        public async Task<HttpResponseMessage> DeleteProject(int id)
        {
            return await http.DeleteAsync($"{route}projects/{id}");
        }

        public async Task<HttpResponseMessage> CreateUser(UserDTO user)
        {
            var jsoncontent = JsonConvert.SerializeObject(user);
            var content = new StringContent(jsoncontent, Encoding.UTF8, "application/json");
            return await http.PostAsync($"{route}users/", content);
        }

        public async Task<UserDTO> GetUser(int id)
        {
            UserDTO user;
            var response = await http.GetAsync($"{route}users/{id}");
            user = JsonConvert.DeserializeObject<UserDTO>(await response.Content.ReadAsStringAsync());
            return user;
        }

        public async Task<HttpResponseMessage> UpdateUser(UserDTO user, int id)
        {
            var content = JsonConvert.SerializeObject(user);
            return await http.PutAsJsonAsync($"{route}users/{id}", user);
        }

        public async Task<HttpResponseMessage> DeleteUser(int id)
        {
            return await http.DeleteAsync($"{route}users/{id}");
        }

        public async Task<HttpResponseMessage> DeleteTask(int id)
        {
            return await http.DeleteAsync($"{route}tasks/{id}");
        }

        public async Task<HttpResponseMessage> CreateTask(TaskDTO task)
        {
            var jsoncontent = JsonConvert.SerializeObject(task);
            var content = new StringContent(jsoncontent, Encoding.UTF8, "application/json");
            return await http.PostAsync($"{route}tasks/", content);
        }

        public async Task<TaskDTO> GetTask(int id)
        {
            TaskDTO task;
            var response = await http.GetAsync($"{route}tasks/{id}");
            task = JsonConvert.DeserializeObject<TaskDTO>(await response.Content.ReadAsStringAsync());
            return task;
        }

        public async Task<HttpResponseMessage> UpdateTask(TaskDTO task, int id)
        {
            var content = JsonConvert.SerializeObject(task);
            return await http.PutAsJsonAsync($"{route}tasks/{id}", task);
        }

        public async Task<HttpResponseMessage> DeleteTeam(int id)
        {
            return await http.DeleteAsync($"{route}teams/{id}");
        }

        public async Task<HttpResponseMessage> CreateTeam(TeamDTO team)
        {
            var jsoncontent = JsonConvert.SerializeObject(team);
            var content = new StringContent(jsoncontent, Encoding.UTF8, "application/json");
            return await http.PostAsync($"{route}teams/", content);
        }

        public async Task<TeamDTO> GetTeam(int id)
        {
            TeamDTO team;
            var response = await http.GetAsync($"{route}teams/{id}");
            team = JsonConvert.DeserializeObject<TeamDTO>(await response.Content.ReadAsStringAsync());
            return team;
        }

        public async Task<HttpResponseMessage> UpdateTeam(TeamDTO team, int id)
        {
            var content = JsonConvert.SerializeObject(team);
            return await http.PutAsJsonAsync($"{route}teams/{id}", team);
        }
    }
}
