﻿using BSA.Client.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BSA.Client.Models.LinqDTOs;

namespace BSA.Client
{
    public class DataProvider
    {
        private static readonly Lazy<DataProvider> Instance = new Lazy<DataProvider>(() => new DataProvider());

        IEnumerable<UserDTO> users;
        IEnumerable<ProjectDTO> projects;
        IEnumerable<TeamDTO> teams;
        IEnumerable<TaskDTO> tasks;
        IEnumerable<SortedUsersDTO> sortedUsers;
        IEnumerable<TeamWithOlderUsersDTO> listOfOlderUsers;
        IEnumerable<TaskDTO> listOfUserTasks;
        Dictionary<ProjectDTO, int> numberOfUserTasks;
        Dictionary<int, string> finishedTasks;
        ProjectTasksInfoDTO projectTasksInfo;
        UserTasksDataDTO userTasksData;

        const string route = "https://localhost:44344/api";
        HttpClient http;

        private DataProvider()
        {
            users = null;
            projects = null;
            teams = null;
            tasks = null;
            sortedUsers = null;
            listOfOlderUsers = null;
            numberOfUserTasks = null;
            finishedTasks = null;
            projectTasksInfo = null;
            listOfUserTasks = null;
            projectTasksInfo = null;
            http = new HttpClient();
        }

        public static DataProvider GetInstance() => Instance.Value;

        public async Task<IEnumerable<UserDTO>> GetUsers()
        {
            using (var response = await http.GetAsync($"{route}/users"))
            {
                users = JsonConvert.DeserializeObject<IEnumerable<UserDTO>>(await response.Content.ReadAsStringAsync());
            }

            return users;
        }

        public async Task<IEnumerable<ProjectDTO>> GetProjects()
        {
            using (var response = await http.GetAsync($"{route}/projects"))
            {
                projects = JsonConvert.DeserializeObject<IEnumerable<ProjectDTO>>(await response.Content.ReadAsStringAsync());
            }

            return projects;
        }

        public async Task<IEnumerable<TeamDTO>> GetTeams()
        {
            using (var response = await http.GetAsync($"{route}/teams"))
            {
                teams = JsonConvert.DeserializeObject<IEnumerable<TeamDTO>>(
                    await response.Content.ReadAsStringAsync());
            }

            return teams;
        }

        public async Task<IEnumerable<TaskDTO>> GetTasks()
        {
            using (var response = await http.GetAsync($"{route}/tasks"))
            {
                tasks = JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(
                    await response.Content.ReadAsStringAsync());
            }

            return tasks;
        }

        public async Task<ProjectTasksInfoDTO> GetTasksInfoForProject(int projectId)
        {
            using (var response = await http.GetAsync($"{route}/data/projectTasks/{projectId}"))
            {
                projectTasksInfo = JsonConvert.DeserializeObject<ProjectTasksInfoDTO>(
                    await response.Content.ReadAsStringAsync());
            }

            return projectTasksInfo;
        }

        public async Task<IEnumerable<SortedUsersDTO>> GetSortedUsers()
        {
            using (var response = await http.GetAsync($"{route}/data/sortedUsers"))
            {
                sortedUsers = JsonConvert.DeserializeObject<IEnumerable<SortedUsersDTO>>(
                    await response.Content.ReadAsStringAsync());
            }

            return sortedUsers;
        }

        public async Task<IEnumerable<TeamWithOlderUsersDTO>> GetListOfOlderUsers()
        {
            using (var response = await http.GetAsync($"{route}/data/olderUsers"))
            {
                listOfOlderUsers = JsonConvert.DeserializeObject<IEnumerable<TeamWithOlderUsersDTO>>(await response.Content.ReadAsStringAsync());
            }

            return listOfOlderUsers;
        }

        public async Task<Dictionary<int, string>> GetFinishedTasks(int userId)
        {
            using (var response = await http.GetAsync($"{route}/data/finishedTasks/{userId}"))
            {
                finishedTasks = JsonConvert.DeserializeObject<Dictionary<int, string>>(
                    await response.Content.ReadAsStringAsync());
            }

            return finishedTasks;
        }

        public async Task<IEnumerable<TaskDTO>> GetListOfUserTasks(int userId)
        {
            using (var response = await http.GetAsync($"{route}/data/userTasks/{userId}"))
            {
                listOfUserTasks = JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(
                    await response.Content.ReadAsStringAsync());
            }

            return listOfUserTasks;
        }

        public async Task<Dictionary<ProjectDTO, int>> GetNumberOfUserTasks(int userId)
        {
            using (var response = await http.GetAsync($"{route}/data/numberOfUserTasks/{userId}"))
            {
                numberOfUserTasks = JsonConvert.DeserializeObject<Dictionary<ProjectDTO, int>>(
                    await response.Content.ReadAsStringAsync());
            }

            return numberOfUserTasks;
        }

        public async Task<UserTasksDataDTO> GetUserTasksData(int userId)
        {
            using (var response = await http.GetAsync($"{route}/data/userTasksData/{userId}"))
            {
                userTasksData = JsonConvert.DeserializeObject<UserTasksDataDTO>(
                    await response.Content.ReadAsStringAsync());
            }

            return userTasksData;
        }
    }
}
