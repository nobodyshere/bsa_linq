﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using BSA.Client.Models;

namespace BSA.Client
{
    class CrudMenu
    {
        private static readonly Lazy<CrudMenu> Instance = new Lazy<CrudMenu>(() => new CrudMenu());

        public static CrudMenu GetInstance() => Instance.Value;
        private CrudProvider crudProvider = CrudProvider.GetInstance();

        private CrudMenu()
        {

        }

        public void MenuStart()
        {
            Console.WriteLine("Please choose an option below");
            Console.WriteLine("1. Projects \n\n" +
                              "2. Tasks \n\n" +
                              "3. Users \n\n" +
                              "4. Teams \n\n");

            int.TryParse(Console.ReadLine(), out int responce);

            Console.Clear();

            switch (responce)
            {
                case 1:
                    GetProjectMenu();
                    break;
                case 2:
                    GetTaskMenu();
                    break;
                case 3:
                    GetUserMenu();
                    break;
                case 4:
                    GetTeamsMenu();
                    break;
                default:
                    DisplayError($"There are no option like {responce}.");
                    break;
            }
        }

        async Task GetProjectMenu()
        {
            Console.WriteLine("Project options:\n" +
                              "1. Create\n" +
                              "2. Get project by id\n" +
                              "3. Update project\n" +
                              "4. Delete project");

            int.TryParse(Console.ReadLine(), out int responce);

            switch (responce)
            {
                case 1:
                    Console.WriteLine("Please enter project name");
                    string name = Console.ReadLine();
                    Console.WriteLine("Please enter project description");
                    string description = Console.ReadLine();
                    Console.WriteLine("Please enter project deadline");
                    DateTime deadline = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter project author id");
                    int authorid = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter project team id");
                    int teamid = Int32.Parse(Console.ReadLine());

                    ProjectDTO project = new ProjectDTO()
                    {
                        Id = 0,
                        AuthorId = authorid,
                        CreatedAt = DateTime.Now,
                        Deadline = deadline,
                        Description = description,
                        Name = name,
                        TeamId = teamid
                    };
                    await crudProvider.CreateProject(project);
                    break;
                case 2:
                    Console.WriteLine("Please enter project id");
                    int projectId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(await crudProvider.GetProject(projectId));
                    break;
                case 3:
                    Console.WriteLine("Please enter project ID you want to update.");
                    projectId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter project name");
                    name = Console.ReadLine();
                    Console.WriteLine("Please enter project description");
                    description = Console.ReadLine();
                    Console.WriteLine("Please enter project deadline");
                    deadline = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter project author id");
                    authorid = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter project team id");
                    teamid = Int32.Parse(Console.ReadLine());
                    project = new ProjectDTO()
                    {
                        AuthorId = authorid,
                        CreatedAt = DateTime.Now,
                        Deadline = deadline,
                        Description = description,
                        Name = name,
                        TeamId = teamid
                    };
                    await crudProvider.UpdateProject(project, projectId);
                    break;
                case 4:
                    Console.WriteLine("Please enter project id you want to delete");
                    projectId = Int32.Parse(Console.ReadLine());
                    await crudProvider.DeleteProject(projectId);
                    break;;
            }
        }

        async Task GetUserMenu()
        {
            Console.WriteLine("User options:\n" +
                              "1. Create\n" +
                              "2. Get user by id\n" +
                              "3. Update user\n" +
                              "4. Delete user");

            int.TryParse(Console.ReadLine(), out int responce);

            switch (responce)
            {
                case 1:
                    Console.WriteLine("Please enter user first name");
                    string firstName = Console.ReadLine();
                    Console.WriteLine("Please enter user second name");
                    string lastName = Console.ReadLine();
                    Console.WriteLine("Please enter user email");
                    string email = Console.ReadLine();
                    Console.WriteLine("Please enter user birthday");
                    DateTime birthDateTime = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter user team id");
                    int teamid = Int32.Parse(Console.ReadLine());
                    UserDTO user = new UserDTO()
                    {
                        Id = 0,
                        Birthday = birthDateTime,
                        Email = email,
                        FirstName = firstName,
                        LastName = lastName,
                        RegisteredAt = DateTime.Now,
                        TeamId = teamid
                    };
                    await crudProvider.CreateUser(user);
                    break;
                case 2:
                    Console.WriteLine("Please enter user id");
                    int userId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(await crudProvider.GetUser(userId));
                    break;
                case 3:
                    Console.WriteLine("Please enter user ID you want to update.");
                    userId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter user first name");
                    firstName = Console.ReadLine();
                    Console.WriteLine("Please enter user second name");
                    lastName = Console.ReadLine();
                    Console.WriteLine("Please enter user email");
                    email = Console.ReadLine();
                    Console.WriteLine("Please enter user birthday");
                    birthDateTime = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter user team id");
                    teamid = Int32.Parse(Console.ReadLine());
                    user = new UserDTO()
                    {
                        Id = userId,
                        Birthday = birthDateTime,
                        Email = email,
                        FirstName = firstName,
                        LastName = lastName,
                        RegisteredAt = DateTime.Now,
                        TeamId = teamid
                    };
                    await crudProvider.UpdateUser(user, userId);
                    break;
                case 4:
                    Console.WriteLine("Please enter user id you want to delete");
                    userId = Int32.Parse(Console.ReadLine());
                    await crudProvider.DeleteUser(userId);
                    break; ;
            }

        }


        async Task GetTaskMenu()
        {
            Console.WriteLine("Task options:\n" +
                              "1. Create\n" +
                              "2. Get task by id\n" +
                              "3. Update task\n" +
                              "4. Delete task");

            int.TryParse(Console.ReadLine(), out int responce);

            switch (responce)
            {
                case 1:
                    Console.WriteLine("Please enter task name");
                    var taskName = Console.ReadLine();
                    Console.WriteLine("Please enter task description");
                    var taskDescription = Console.ReadLine();
                    Console.WriteLine("Please enter task finish date");
                    DateTime finishedAt = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter task state");
                    int taskState = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter task project id");
                    int projectId = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter task performer id");
                    int performerId = int.Parse(Console.ReadLine());
                    TaskDTO task = new TaskDTO()
                    {
                        Id = 0,
                        Name = taskName,
                        Description = taskDescription,
                        CreatedAt = DateTime.Now,
                        FinishedAt = finishedAt,
                        State = (TaskState)taskState,
                        ProjectId = projectId,
                        PerformerId = performerId

                    };
                    await crudProvider.CreateTask(task);
                    break;
                case 2:
                    Console.WriteLine("Please enter task id");
                    int taskId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(await crudProvider.GetTask(taskId));
                    break;
                case 3:
                    Console.WriteLine("Please enter task ID you want to update.");
                    taskId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter task name");
                    taskName = Console.ReadLine();
                    Console.WriteLine("Please enter task description");
                    taskDescription = Console.ReadLine();
                    Console.WriteLine("Please enter task finish date");
                    finishedAt = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter task state");
                    taskState = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter task project id");
                    projectId = int.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter task performer id");
                    performerId = int.Parse(Console.ReadLine());
                    task = new TaskDTO()
                    {
                        Id = 0,
                        Name = taskName,
                        Description = taskDescription,
                        CreatedAt = DateTime.Now,
                        FinishedAt = finishedAt,
                        State = (TaskState)taskState,
                        ProjectId = projectId,
                        PerformerId = performerId

                    };
                    await crudProvider.UpdateTask(task, taskId);
                    break;
                case 4:
                    Console.WriteLine("Please enter task id you want to delete");
                    taskId = Int32.Parse(Console.ReadLine());
                    await crudProvider.DeleteTask(taskId);
                    break; ;
            }

        }

        async Task GetTeamsMenu()
        {
            Console.WriteLine("Team options:\n" +
                              "1. Create\n" +
                              "2. Get task by id\n" +
                              "3. Update task\n" +
                              "4. Delete task");

            int.TryParse(Console.ReadLine(), out int responce);

            switch (responce)
            {
                case 1:
                    Console.WriteLine("Please enter team name");
                    var teamName = Console.ReadLine();
                    TeamDTO team = new TeamDTO()
                    {
                        Id = 0,
                        Name = teamName,
                        CreatedAt = DateTime.Now
                    };
                    await crudProvider.CreateTeam(team);
                    break;
                case 2:
                    Console.WriteLine("Please enter team id");
                    int teamId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine(await crudProvider.GetTeam(teamId));
                    break;
                case 3:
                    Console.WriteLine("Please enter team ID you want to update.");
                    teamId = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Please enter team name");
                    teamName = Console.ReadLine();
                    team = new TeamDTO()
                    {
                        Id = 0,
                        Name = teamName,
                        CreatedAt = DateTime.Now
                    };
                    await crudProvider.UpdateTeam(team, teamId);
                    break;
                case 4:
                    Console.WriteLine("Please enter team id you want to delete");
                    teamId = Int32.Parse(Console.ReadLine());
                    await crudProvider.DeleteTeam(teamId);
                    break; ;
            }

        }

        private void DisplayError(string message)
        {
            var current = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write($"ERROR: ");
            Console.ForegroundColor = current;
            Console.WriteLine(message);
        }
    }
}