﻿using Newtonsoft.Json;

namespace BSA.Client.Models
{
    public class ProjectDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public System.DateTime CreatedAt { get; set; }
        [JsonProperty("deadline")]
        public System.DateTime Deadline { get; set; }
        [JsonProperty("author_id")]
        public int AuthorId { get; set; }
        [JsonProperty("team_id")]
        public int TeamId { get; set; }

        public override string ToString()
        {
            return $"ID: {Id},\n" +
                   $"Name: {Name},\n" +
                   $"Description: {Description},\n" +
                   $"Created: {CreatedAt.ToShortDateString()},\n" +
                   $"Deadline: {Deadline.ToShortDateString()},\n" +
                   $"Author_id: {AuthorId},\n" +
                   $"Team_id ID: {TeamId}";
        }
    }
}
