﻿using Newtonsoft.Json;

namespace BSA.Client.Models
{
    public class TeamDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("created_at")]
        public System.DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return $"ID: {Id},\n" +
                   $"Name: {Name},\n" +
                   $"Created: {CreatedAt.ToShortDateString()}";
        }
    }
}
