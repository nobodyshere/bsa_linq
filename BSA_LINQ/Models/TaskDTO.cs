﻿using Newtonsoft.Json;

namespace BSA.Client.Models
{
    public class TaskDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public System.DateTime CreatedAt { get; set; }
        [JsonProperty("finished_at")]
        public System.DateTime FinishedAt { get; set; }
        [JsonProperty("state")]
        public TaskState State { get; set; }
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }
        [JsonProperty("performer_id")]
        public int PerformerId { get; set; }

        public override string ToString()
        {
            return $"ID: {Id},\n" +
                   $"Name: {Name},\n" +
                   $"Description: {Description},\n" +
                   $"Created: {CreatedAt.ToShortDateString()},\n" +
                   $"Finished: {FinishedAt.ToShortDateString()},\n" +
                   $"State: {State},\n" +
                   $"Project ID: {ProjectId},\n" +
                   $"PerformerId: {PerformerId}";
        }
    }
}
