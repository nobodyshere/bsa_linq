﻿using Newtonsoft.Json;

namespace BSA.Client.Models
{
    public class UserDTO
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("birthday")]
        public System.DateTime Birthday { get; set; }
        [JsonProperty("registered_at")]
        public System.DateTime RegisteredAt { get; set; }
        [JsonProperty("team_id")]
        public int? TeamId { get; set; }

        public override string ToString()
        {
            return $"ID: {Id},\n" +
                   $"First_Name: {FirstName},\n" +
                   $"Last_Name: {LastName},\n" +
                   $"Email: {Email},\n" +
                   $"Birthd_at: {Birthday.ToShortDateString()},\n" +
                   $"Registered_at: {RegisteredAt.ToShortDateString()},\n" +
                   $"Team_Id: {TeamId}";
        }
    }
}
