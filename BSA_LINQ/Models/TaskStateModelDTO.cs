﻿namespace BSA.Client.Models
{
    [System.Serializable]
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished,
        Cancelled
    }
}
