﻿namespace BSA.Client.Models.LinqDTOs
{
    public class UserTasksDataDTO
    {
        public UserDTO user;
        public ProjectDTO lastProject;
        public int lastProjectTasksCount;
        public int unfinishedTasksCount;
        public TaskDTO longestTaskByDate;
    }
}
