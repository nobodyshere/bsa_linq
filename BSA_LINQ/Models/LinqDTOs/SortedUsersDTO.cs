﻿using System.Collections.Generic;

namespace BSA.Client.Models.LinqDTOs
{
    public class SortedUsersDTO
    {
        public UserDTO user;
        public List<TaskDTO> tasks;
    }
}
