﻿using System.Collections.Generic;

namespace BSA.Client.Models.LinqDTOs
{
    public class TeamWithOlderUsersDTO
    {
        public int teamId;
        public string teamName;
        public List<UserDTO> users;
    }
}
