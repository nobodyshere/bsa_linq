﻿

namespace BSA.Client.Models.LinqDTOs
{
    public class ProjectTasksInfoDTO
    {
        public ProjectDTO project;
        public TaskDTO longestTask;
        public TaskDTO shortestTask;
        public int usersCount;
    }
}
